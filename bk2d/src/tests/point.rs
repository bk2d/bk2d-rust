use crate::point::*;

#[test]
fn merge() {
    let a = Path::new(-1, -1);
    let b = Path::merge(&[Path::right(), Path::up(), Path::left(), Path::left()]);
    assert_eq!(a, b);
}

#[test]
fn length() {
    assert_eq!(Path::new(5, -8).length(), 13);
    assert_eq!(Point::new(2, 3).length(), 5);
}

#[test]
fn add_path_path() {
    let a = Path::new(1, 2);
    let b = Path::new(2, -1);
    let r = Path::new(3, 1);
    assert_eq!(a + b, r);
}

#[test]
fn add_point_path() {
    let a = Point::new(1, 2);
    let b = Path::new(2, -1);
    let r = Point::new(3, 1);
    assert_eq!(a + b, r);
}

#[test]
fn sub_point_path() {
    let a = Point::new(3, 2);
    let b = Path::new(2, -1);
    let r = Point::new(1, 3);
    assert_eq!(a - b, r);
}

#[test]
fn sub_point_point() {
    let from = Point::new(2, 4);
    let to = Point::new(3, 2);
    let p = Path::new(1, -2);
    assert_eq!(to - from, p);
}

#[test]
fn inside() {
    let board = Board::new(5, 5);

    let i = Point::new(0, 4);
    assert!(board.inside(i));
    assert!(i.inside(board));

    let out = Point::new(5, 3);
    assert!(board.outside(out));
    assert!(out.outside(board));
}
