use crate::Player;

#[derive(Clone, Debug, Default)]
pub struct Players<P> {
    pub players: Vec<P>,
    pub current: Option<usize>,
}

impl<P> Players<P> {
    pub fn new() -> Players<P> {
        Players {
            players: Vec::new(),
            current: None,
        }
    }

    pub fn sure_start(&self, msg: &str) {
        if self.current.is_none() {
            panic!("{}", msg);
        }
    }

    pub fn not_start(&self, msg: &str) {
        if self.current.is_some() {
            panic!("{}", msg);
        }
    }

    pub fn start(&mut self) {
        self.not_start("Can't start twice");
        self.current = Some(0);
    }

    pub fn is_start(&self) -> bool {
        self.current.is_some()
    }

    pub fn add(&mut self, p: P) {
        self.players.push(p);
    }

    pub fn remove(&mut self, i: usize) {
        self.players.remove(i);
        let player = self.current.as_mut().unwrap();
        if i < *player {
            *player -= 1;
        }
    }

    pub fn current_index(&self) -> usize {
        self.current.expect("Not current player before start")
    }

    pub fn current_ref(&self) -> &P {
        &self.players[self.current_index()]
    }

    pub fn current_mut(&mut self) -> &mut P {
        self.sure_start("Not current player before start");
        &mut self.players[self.current.unwrap()]
    }

    pub fn next_player(&mut self) -> usize {
        self.sure_start("Not next player before start");
        let player = self.current.as_mut().unwrap();
        *player += 1;
        if *player == self.players.len() {
            *player = 0;
        }
        *player
    }
}

impl<P: Player> Players<P> {
    pub fn current_ident(&self) -> &<P as Player>::Ident {
        &self.players[self.current_index()].ident()
    }
}

#[derive(Clone, Debug)]
pub struct SimplePublic<K, T> {
    pub kind: K,
    pub data: T,
    pub round: u32,
    pub id: u64,
}

impl<K, T> crate::Public for SimplePublic<K, T> {
    fn round(&self) -> u32 {
        self.round
    }

    fn id(&self) -> u64 {
        self.id
    }
}
