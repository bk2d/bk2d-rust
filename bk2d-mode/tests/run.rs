use bk2d::{
    point::{Path, Point},
    BK2D,
};
use bk2d_mode::origin::{self, Action::*, Origin};

#[test]
fn test() {
    let mut game = Origin::new(Default::default(), Default::default());
    game.add_player(origin::Player::new("a".to_string(), Point::new(2, 3)))
        .expect("Fail to add Player a");
    game.add_player(origin::Player::new("b".to_string(), Point::new(0, 0)))
        .expect("Fail to add Player b");
    game.start();
    let mut pla;
    pla = game.current_player();
    assert_eq!(pla, "a");
    game.action(Walk(Path { dx: -1, dy: 0 }))
        .expect("a fail to walk");
    pla = game.next_player();
    assert_eq!(pla, "b");
    game.action(Run(Path { dx: 0, dy: 2 }))
        .expect("b fail to run");
    pla = game.next_player();
    assert_eq!(pla, "a");
    game.action(Run(Path { dx: 0, dy: -2 }))
        .expect("a fail to run");
    pla = game.next_player();
    assert_eq!(pla, "b");
    game.action(Walk(Path { dx: 1, dy: 0 }))
        .expect("b fail to walk");
    game.action(Attack(Path { dx: 0, dy: -1 }))
        .expect("b fail to attack");
    assert_eq!(game.win().unwrap(), "b");
}
